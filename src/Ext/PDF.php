<?php 
namespace Giahao9899\FileToText\Ext;

use Giahao9899\FileToText\Ext\IConverter;
use Smalot\PdfParser\Parser;

class PDF implements IConverter
{
	private $path;

	function __construct($path)
	{
		$this->path = $path;
	}

	public function text() {
	    $PDFParser = new Parser();
        $pdf = $PDFParser->parseFile($this->path);
        $text = $pdf->getText();

        return $text;
	}
}