<?php 
namespace Giahao9899\FileToText\Ext;

use Giahao9899\FileToText\Ext\IConverter;

class ODT implements IConverter
{
	
	private $path;

	function __construct($path)
	{
		$this->path = $path;
	}

	public function text()
	{
    $out_text = '';
		$dataFile = "content.xml";     
       
    //Create a new ZIP archive object
    $zip = new \ZipArchive;
 
    // Open the archive file
    if (true === $zip->open($this->path)) {
        // If successful, search for the data file in the archive
        if (($index = $zip->locateName($dataFile)) !== false) {
            // Index found! Now read it to a string
            $text = $zip->getFromIndex($index);
            // Load XML from a string
            // Ignore errors and warnings
            $xml = \DOMDocument::loadXML($text, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
            // Remove XML formatting tags and return the text
            $out_text =  strip_tags($xml->saveXML());
        }
        //Close the archive file
        $zip->close();
    }

    return $out_text;
	}
}
