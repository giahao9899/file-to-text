<?php 
namespace Giahao9899\FileToText\Ext;

use Giahao9899\FileToText\Ext\IConverter;

class XLSX implements IConverter
{
	
	private $path;

	function __construct($path)
	{
		$this->path = $path;
	}

	public function text()
	{
		
		$xml_filename = "xl/sharedStrings.xml"; //content file name

	    $zip_handle = new \ZipArchive;

	    $output_text = "";

	    if(true === $zip_handle->open($this->path)){

	        if(($xml_index = $zip_handle->locateName($xml_filename)) !== false) {

	            $xml_datas = $zip_handle->getFromIndex($xml_index);
	            $xml_handle = new \DOMDocument();
	            $xml_handle->loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);

	            $output_text = strip_tags($xml_handle->saveXML());
	        }else{
	            $output_text .="";
	        }
	        $zip_handle->close();
	    }else{
	    $output_text .= "";
	    }
	    return $output_text;
	}
}
 ?>