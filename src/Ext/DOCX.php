<?php 
namespace Giahao9899\FileToText\Ext;

use Giahao9899\FileToText\Ext\IConverter;

class DOCX implements IConverter
{
	
	private $path;

	function __construct($path)
	{
		$this->path = $path;
	}

	public function text()
	{
		
		$output_text = '';
        $content = '';

        $zip = zip_open($this->path);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $output_text = strip_tags($content);

        return  html_entity_decode($output_text);
	}
}