<?php 
	namespace Giahao9899\FileToText;

	use Giahao9899\FileToText\Ext\PDF;
	use Giahao9899\FileToText\Ext\XLSX;
	use Giahao9899\FileToText\Ext\DOCX;
	use Giahao9899\FileToText\Ext\DOC;
	use Giahao9899\FileToText\Ext\PPTX;
	use Giahao9899\FileToText\Ext\ODT;
	/**
	 * 
	 */
	class Converter
	{
		private $path;
		private $fileExt;

		function __construct($path) {
			$this->setPath($path);
		}

		public function setPath($path)
		{
			if(!file_exists($path)) {
				throw new \Exception('File Not exists');
			}
			
			$fileExt = pathinfo($path, PATHINFO_EXTENSION);

			$this->path = $path;
			$this->fileExt = $fileExt;

		}

		public function getHandler()
		{
			$fileExt = $this->fileExt;
			$path = $this->path;
			$handler = null;

			switch ($fileExt) {
				case 'doc':
					$handler =  new DOC($path);
					break;
				case 'docx':
					$handler = new DOCX($path);
					break;
				case 'pdf':
					$handler = new PDF($path);
					break;
				case 'xlsx':
					$handler = new XLSX($path);
					break;
				case 'pptx':
					$handler = new PPTX($path);
					break;
				case 'odt':
					$handler = new ODT($path);
					break;
				default:
					throw new \Exception('Invalid File Type');
			}

			return $handler;
		}
	}
 ?>