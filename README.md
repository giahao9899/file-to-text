### File to Text
- This is a package which can extract file(docx, pdf, xlsx,...)

## Install

```
	composer require giahao9899/filetotext
```

## Usage

```php
	$converter = new Converter($path);
	$text = ($converter->getHandler())->text(); 
```