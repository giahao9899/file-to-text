<?php 
	namespace Giahao9899\FileToText\Tests;

	use PHPUnit\Framework\TestCase;
	use Giahao9899\FileToText\Converter;
	
	class ConverterTest extends TestCase
	{
		private $pathPDFTest = __DIR__ . '/testfile/pdf-test.pdf';
		private $pathDOCXTest = __DIR__ . '/testfile/docx-test.docx';
		private $pathXLSXTest =  __DIR__ . '/testfile/xlsx-test.xlsx';
		private $pathODTTest =  __DIR__ . '/testfile/odt-test.odt';
		private $textTest = "\rABCDEF\r\n1234567890\r\n*&^%\r\n";

		/** @test */
		public function TestItExtractTextFromDOCX()
		{
			$converter = new Converter($this->pathDOCXTest);
			$text = ($converter->getHandler())->text();

			$this->assertSame($this->textTest, $text);
		}

		/** @test */
		public function TestItExtractTextFromPDF()
		{
			$converter = new Converter($this->pathPDFTest);
			$text = ($converter->getHandler())->text();

			$this->assertSame($this->textTest, $text);
		}

		/** @test */
		public function TestItExtractTextFromXLSX()
		{
			$converter = new Converter($this->pathXLSXTest);
			$text = ($converter->getHandler())->text();

			$this->assertSame($this->textTest, $text);
		}

		/** @test */
		public function TestItExtractTextFromODT()
		{
			$converter = new Converter($this->pathODTTest);
			$text = ($converter->getHandler())->text();

			$this->assertSame($this->textTest, $text);
		}
		
	}
 ?>